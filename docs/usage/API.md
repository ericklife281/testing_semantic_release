# Table of content

<!-- TOC -->

- [Table of content](#table-of-content)
    - [All Infra Request Types with Parameters](#all-infra-request-types-with-parameters)
    - [Create Infra Request](#create-infra-request)
    - [List InfraRequest Main Processing Queue](#list-infrarequest-main-processing-queue)
    - [Trigger Pipeline (re-run)](#trigger-pipeline-re-run)
    - [Gitlab Runner Trace (log)](#gitlab-runner-trace-log)
    - [List InfraRequest ID Processing Queue (log)](#list-infrarequest-id-processing-queue-log)
    - [Update IaC State (upload infra state file to backend)](#update-iac-state-upload-infra-state-file-to-backend)
    - [Update CaC Results (Ansible output)](#update-cac-results-ansible-output)
    - [Update IaC Result (IaC process result to backend)](#update-iac-result-iac-process-result-to-backend)

<!-- /TOC -->

## All Infra Request Types with Parameters

| Method(s) | URL                               |
|-----------|-----------------------------------|
| GET       | /api/infrarequest/parameters/list |

Sampe Response:

    {
        "ec2": {
            "name": "EC2 Instance", 
            "parameters": {
            "aws_ami": {
                "label": "AWS AMI Code", 
                "type": "string"
            }, 
            "aws_ebs_data_size_1": {
                "label": "aws_ebs_data_size_1", 
                "type": "integer"
            }, 
            "aws_ec2_count": {
                "label": "EC2 Count", 
                "type": "integer"
            }, 
            "aws_ec2_hostname": {
                "label": "AWS EC2 Hostname", 
                "type": "string"
            }, 
            "aws_instance_type": {
                "items": {
                "m5.large": "m5.large", 
                "m5.small": "m5.small", 
                "t2.large": "t2.large", 
                "t2.small": "t2.small"
                }, 
                "label": "AWS Instance Type", 
                "type": "select"
            }, 
            "aws_security_group": {
                "label": "AWS Security Group", 
                "type": "string"
            }, 
            "aws_subnet": {
                "label": "AWS Subnet", 
                "type": "string"
            }, 
            "environment": {
                "items": {
                "nonprod": "Non-Prod", 
                "preprod": "Pre-Prod", 
                "prod": "Prod"
                }, 
                "label": "Target Environment", 
                "type": "select"
            }, 
            "product_code": {
                "label": "Product Code", 
                "type": "string"
            }, 
            "product_name": {
                "label": "Product Name", 
                "type": "string"
            }, 
            "region": {
                "items": {
                "us-east-1": "us-east-1", 
                "us-west-2": "us-west-2"
                }, 
                "label": "AWS Region", 
                "type": "select"
            }
            }
        }, 
        "test_pbd": {
            "name": "PBD Pipeline Test", 
            "parameters": {
            "product_code": {
                "label": "Product Code", 
                "type": "string"
            }, 
            "product_name": {
                "label": "Product Name", 
                "type": "string"
            }, 
            "region": {
                "items": {
                "us-east-1": "us-east-1", 
                "us-west-2": "us-west-2"
                }, 
                "label": "AWS Region", 
                "type": "select"
            }
            }
        }
    }

## Create Infra Request

| Method(s) | URL                      | Content type     |
|-----------|--------------------------|------------------|
| POST      | /api/infrarequest/create | application/json |

Sample Request:

    {
        "request_type": "ec2",
        "user_id": "1",
        "region": "us-east-1",
        "product_name": "PRD1234",
        "product_code": "PRD1234",
        "environment": "nonprod",
        "aws_instance_type": "t2.small",
        "aws_ami": "PRD1750.pxcnode.OL7.7.v1.1",
        "aws_ec2_count": "1",
        "aws_ec2_hostname": "pdbtest",
        "aws_ebs_data_size_1": "20",
        "aws_subnet": "dv-3.dev",
        "aws_security_group": "dev.db"
    }

Error Response:

    {
        "error": "Invalid Request Type"
    }

Success Response :

    {
        "uuid": "9b5cfb40-16ac-4dbe-9c5e-ef84f554c8ff"
    }

## List InfraRequest Main Processing Queue

| Method(s) | URL                      | Content type     |
|-----------|--------------------------|------------------|
| GET       | /api/infrarequest/create | application/json |

Sample Response :

    [ 
        {
        "commit_id": null, 
        "date_created": "Fri, 18 Mar 2022 20:08:57 GMT", 
        "finish_time": null, 
        "gitlab_job_link": null, 
        "id": 51, 
        "infra_parameters": null, 
        "process_name": "AWS-TEST", 
        "process_result": 0, 
        "process_status": "Pending", 
        "product_code": "", 
        "product_name": "", 
        "region": "", 
        "slug": "", 
        "start_time": null, 
        "user_id": 1
        }, 
        {
        "commit_id": null, 
        "date_created": "Sat, 26 Mar 2022 01:07:31 GMT", 
        "finish_time": null, 
        "gitlab_job_link": null, 
        "id": 61, 
        "infra_parameters": "{\"request_type\": \"ec2\", \"user_id\": \"1\", \"region\": \"us-east-1\", \"product_name\": \"PRD1234\", \"product_code\": \"PRD1234\", \"environment\": \"nonprod\", \"aws_instance_type\": \"t2.small\", \"aws_ami\": \"PRD1750.pxcnode.OL7.7.v1.1\", \"aws_ec2_count\": \"1\", \"aws_ec2_hostname\": \"pdbtest\", \"aws_ebs_data_size_1\": \"20\", \"aws_subnet\": \"dv-3.dev\", \"aws_security_group\": \"dev.db\"}", 
        "process_name": "ec2", 
        "process_result": 0, 
        "process_status": "Generating", 
        "product_code": "PRD1234", 
        "product_name": "PRD1234", 
        "region": "us-east-1", 
        "slug": "9b5cfb40-16ac-4dbe-9c5e-ef84f554c8ff", 
        "start_time": null, 
        "user_id": 1
        }
    ]

## Trigger Pipeline (re-run)

| Method(s) | URL                     | Content type     |
|-----------|-------------------------|------------------|
| POST      | /api/infrarequest/rerun | application/json |

Sample Request:

    {
        "user_id": "1",
        "slug": "9b5cfb40-16ac-4dbe-9c5e-ef84f554c8ff", 
    }

Error Response:

    {
        "error": "Pipeline Error Message"
    }

Success Response :

    {
        "id": "a6c1e85f3411cd80a80dddfbbefef386c0f3e399",
        "last_pipeline": null,
        "short_id": "a6c1e85f",
        "web_url": "https://git.tmaws.io/dbre/pbd/terraform/-/commit/a6c1e85f3411cd80a80dddfbbefef386c0f3e399"
    }

## Gitlab Runner Trace (log)

| Method(s) | URL                          | Response type |
|-----------|------------------------------|---------------|
| GET       | /api/infrarequest/log/<slug> | text          |

Sample Response

    [0KRunning with gitlab-runner 13.12.0-tm (c7989fb5)
    [0;m[0K  on bootstrapped-gitlab-runner T_y7ton7
    [0;msection_start:1648047562:resolve_secrets
    [0K[0K[36;1mResolving secrets[0;m
    [0;msection_end:1648047562:resolve_secrets
    [0Ksection_start:1648047562:prepare_executor
    [0K[0K[36;1mPreparing the "kubernetes" executor[0;m
    [0;m[0K"PodAnnotations" "iam.amazonaws.com/role" overwritten with "arn:aws:iam::141148610179:role/GitRunner"
    [0;m[0KUsing Kubernetes namespace: prd2381
    [0;m[0KUsing Kubernetes executor with image tmhub.io/ticketmaster/terraformer:v0.13_cd ...
    [0;msection_end:1648047562:prepare_executor
    [0Ksection_start:1648047562:prepare_script
    [0K[0K[36;1mPreparing environment[0;m
    [0;mWaiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Waiting for pod prd2381/runner-ty7ton7-project-40583-concurrent-0vfmzm to be running, status is Pending
        ContainersNotReady: "containers with unready status: [build helper]"
        ContainersNotReady: "containers with unready status: [build helper]"
    Running on runner-ty7ton7-project-40583-concurrent-0vfmzm via bootstrapped-gitlab-runner-b6bd7f4f6-nfgql...
    section_end:1648047599:prepare_script
    [0Ksection_start:1648047599:get_sources
    [0K[0K[36;1mGetting source from Git repository[0;m
    [0;m[32;1mFetching changes with git depth set to 50...[0;m
    Initialized empty Git repository in /builds/T_y7ton7/0/dbre/pbd/terraform/.git/
    [32;1mCreated fresh repository.[0;m
    [32;1mChecking out 76fa14ac as ec2...[0;m

    [32;1mSkipping Git submodules setup[0;m
    section_end:1648047601:get_sources
    [0Ksection_start:1648047601:step_script
    [0K[0K[36;1mExecuting "step_script" stage of the job script[0;m
    [0;m[32;1m$ cd terraform_ec2[0;m
    [32;1m$ terraformer ${ENV_PATH}/sample_pbd_request/ ${TF_ARG} ${TF_VAR}[0;m
    [terraformer] 2022/03/23 15:00:02 Account: tm-nonprod-prd1748
    [terraformer] 2022/03/23 15:00:02 Verifying if the S3 Bucket terraform.eu-west-1.tm-nonprod-prd1748.tmaws exists in eu-west-1...
    [terraformer] 2022/03/23 15:00:03 Verifying if the DynamoDB Table terraformer_lock_table exists in eu-west-1...
    [terraformer] 2022/03/23 15:00:03 Storing Terraform state file in s3://terraform.eu-west-1.tm-nonprod-prd1748.tmaws/PRD1750/tm-nonprod/sample_pbd_request/pbd-node.tfstate
    [terraformer] 2022/03/23 15:00:03 Locking Terraform state file using Dynamodb table terraformer_lock_table / eu-west-1 / tm-nonprod-prd1748
    [terraformer] 2022/03/23 15:00:03 Configuring Terraform Backend to the standard S3 Terraform State File bucket...
    [terraformer] shell - 2022/03/23 15:00:03 Running command: terraform init -backend-config=bucket=terraform.eu-west-1.tm-nonprod-prd1748.tmaws -backend-config=workspace_key_prefix=PRD1750/tm-nonprod/sample_pbd_request/pbd-node/workspace -backend-config=key=PRD1750/tm-nonprod/sample_pbd_request/pbd-node.tfstate -backend-config=region=eu-west-1 -backend-config=profile=tm-nonprod-prd1748-Ops-Techops -backend-config=dynamodb_table=terraformer_lock_table -lock-timeout=10m -plugin-dir=/bin
    [0m[1mInitializing modules...[0m
    Downloading git::https://git.tmaws.io/AWS/terraform-module-naming.git for naming...

    - naming in .terraform/modules/naming
    Downloading git::https://git.tmaws.io/AWS/terraform-module-networks.git for networks...

    - networks in .terraform/modules/networks
    [0m[1mInitializing the backend...[0m
    [0m[32m
    Successfully configured the backend "s3"! Terraform will automatically
    use this backend unless the backend configuration changes.[0m

    [0m[1mInitializing provider plugins...[0m

    - Finding latest version of hashicorp/aws...
    - Installing hashicorp/aws v3.53.0...
    - Installed hashicorp/aws v3.53.0 (unauthenticated)
    The following providers do not have any version constraints in configuration,
    so the latest version was installed.

    To prevent automatic upgrades to new major versions that may contain breaking
    changes, we recommend adding version constraints in a required_providers block
    in your configuration, with the constraint strings suggested below.

    - hashicorp/aws: version = "~> 3.53.0"
    [0m[1m[32mTerraform has been successfully initialized![0m[32m[0m
    [0m[32m
    You may now begin working with Terraform. Try running "terraform plan" to see
    any changes that are required for your infrastructure. All Terraform commands
    should now work.

    If you ever set or change modules or backend configuration for Terraform,
    rerun this command to reinitialize your working directory. If you forget, other
    commands will detect it and remind you to do so if necessary.[0m
    [terraformer] shell - 2022/03/23 15:00:06 Running command: terraform version -json
    [terraformer] 2022/03/23 15:00:07 [telemetry] Sent data to https://tfstats.tmaws.io
    [terraformer] shell - 2022/03/23 15:00:07 Running command: terraform apply -lock-timeout=10m -var-file=tm-nonprod/sample_pbd_request/terraform.tfvars -auto-approve
    [0m[1mdata.aws_security_group.onprem_security_group: Refreshing state...[0m
    [0m[1mdata.aws_vpcs.vpc: Refreshing state...[0m
    [0m[1mmodule.naming.data.aws_caller_identity.current: Refreshing state...[0m
    [0m[1mdata.aws_security_group.db_security_group: Refreshing state...[0m
    [0m[1mdata.aws_subnet.app_subnet: Refreshing state...[0m
    [0m[1mdata.aws_ami.ami_version: Refreshing state...[0m
    [0m[1maws_launch_configuration.test: Creating...[0m[0m
    [0m[1maws_launch_configuration.test: Creation complete after 2s [id=PRD1750.dev1.pbd-node._test][0m[0m
    [0m[1maws_autoscaling_group.test: Creating...[0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [10s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [20s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [30s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [40s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [50s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [1m0s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Still creating... [1m10s elapsed][0m[0m
    [0m[1maws_autoscaling_group.test: Creation complete after 1m11s [id=PRD1750.dev1.pbd-node._test][0m[0m
    [33m
    [1m[33mWarning: [0m[0m[1mValue for undeclared variable[0m

    [0mThe root module does not declare a variable named "instance_nane" but a value
    was found in file "tm-nonprod/sample_pbd_request/terraform.tfvars". To use
    this value, add a "variable" block to the configuration.

    Using a variables file to set an undeclared variable is deprecated and will
    become an error in a future release. If you wish to provide certain "global"
    settings to all configurations in your organization, use TF_VAR_...
    environment variables to set these instead.
    [0m[0m
    [0m[1m[32m
    Apply complete! Resources: 2 added, 0 changed, 0 destroyed.[0m
    Releasing state lock. This may take a few moments...
    section_end:1648047687:step_script
    [0Ksection_start:1648047687:cleanup_file_variables
    [0K[0K[36;1mCleaning up file based variables[0;m
    [0;msection_end:1648047688:cleanup_file_variables
    [0K[32;1mJob succeeded
    [0;m 

## List InfraRequest ID Processing Queue (log)

| Method(s) | URL                              | Content type |
|-----------|----------------------------------|--------------|
| GET       | /api/infrarequest/history/<slug> | json         |

    [
        {
            "date_created": "Fri, 01 Apr 2022 01:50:24 GMT", 
            "id": 291, 
            "infra_request_id": 81, 
            "log_text": "Status changed to Generating", 
            "log_type": "Status"
        }, 
        {
            "date_created": "Fri, 01 Apr 2022 02:06:02 GMT", 
            "id": 301, 
            "infra_request_id": 81, 
            "log_text": "Status changed to Generating", 
            "log_type": "Status"
        }, 
        {
            "date_created": "Fri, 01 Apr 2022 11:46:53 GMT", 
            "id": 311, 
            "infra_request_id": 81, 
            "log_text": "Status changed to Generating", 
            "log_type": "Status"
        }, 
        {
            "date_created": "Fri, 01 Apr 2022 11:54:24 GMT", 
            "id": 321, 
            "infra_request_id": 81, 
            "log_text": "Status changed to Generating", 
            "log_type": "Status"
        }, 
        {
            "date_created": "Fri, 01 Apr 2022 11:56:48 GMT", 
            "id": 331, 
            "infra_request_id": 81, 
            "log_text": "Status changed to Generating", 
            "log_type": "Status"
        }, 
        {
            "date_created": "Fri, 01 Apr 2022 11:56:49 GMT", 
            "id": 341, 
            "infra_request_id": 81, 
            "log_text": "{\"id\": \"a6c1e85f3411cd80a80dddfbbefef386c0f3e399\", \"last_pipeline\": null, \"short_id\": \"a6c1e85f\", \"web_url\": \"https://git.tmaws.io/dbre/pbd/terraform/-/commit/a6c1e85f3411cd80a80dddfbbefef386c0f3e399\"}", 
            "log_type": "Commit"
        }, 
        {
            "date_created": "Fri, 01 Apr 2022 11:56:49 GMT", 
            "id": 351, 
            "infra_request_id": 81, 
            "log_text": "Status changed to Committed", 
            "log_type": "Status"
        }
    ]

## Update IaC State (upload infra state file to backend)

| Method(s) | URL                                       | Payload | Response type |
|-----------|-------------------------------------------|---------|---------------|
| POST      | /api/iac/update/<stack_name>/<process_id> | json    | json          |

Sample Response :

    {
        "status": "ok",
        "trx_id": "9b5cfb40-16ac-4dbe-9c5e-ef84f554c8ff", 
    }

Error Response:

    {
        "status: "error",
        "message": "Invalid payload"
    }

## Update CaC Results (Ansible output)

| Method(s) | URL                                       | Payload | Response type |
|-----------|-------------------------------------------|---------|---------------|
| POST      | /api/cac/update/<stack_name>/<process_id> | json    | json          |

Sample Response:

    {
        "status": "ok",
        "trx_id": "9b5cfb40-16ac-4dbe-9c5e-ef84f554c8ff", 
    }

Error Response:

    {
        "status: "error",
        "message": "Invalid payload"
    }

## Update IaC Result (IaC process result to backend)

| Method(s) | URL                                       | Payload | Response type |
|-----------|-------------------------------------------|---------|---------------|
| POST      | /api/cac/update/<stack_name>/<process_id> | json    | json          |

Sample Payload for **remove-node** command :

    {
        "command": "remove-node",
        "removed_ip_address": "10.156.130.110", 
    }

Sample Payload for **add-node** command :

    {
        "command": "add-node",
        "datacenters: 
            {
                "us-east-1-dev-dc1":
                [
                    "10.156.130.110",
                    "10.156.130.111",
                    "10.156.130.105",
                    "10.156.130.98",
                    "10.156.130.190",
                    "10.156.133.210",
                ]
            }   
    }

Sample Response :

    {
        "status": "ok",
        "trx_id": "9b5cfb40-16ac-4dbe-9c5e-ef84f554c8ff", 
    }

Error Response:

    {
        "status: "error",
        "message": "Invalid payload"
    }
