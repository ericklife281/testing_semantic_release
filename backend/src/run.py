from flask import Flask

app = Flask(__name__)

@app.route('/')
@swag_from('./docs/api/register/hello.yaml')
def hello_world():
    return 'Hello world'

@app.route('/firstname')
@swag_from('./docs/api/register/firstname.yaml')
def firstname():
    return 'firstname'


@app.route('/lastname')
@swag_from('./docs/api/register/lastname.yaml')
def lastname():
    return 'lastname'


@app.route('/phone')
@swag_from('./docs/api/register/phone.yaml')
def lastname():
    return 'phone'

@app.route('/address')
def lastname():
    return 'adresss'

