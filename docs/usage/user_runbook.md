# Table of content
<!-- TOC -->

- [Table of content](#table-of-content)
    - [Additional Dependency Versions](#additional-dependency-versions)
    - [Pre-requisites](#pre-requisites)
    - [Setup and Pre-Check for Mac](#setup-and-pre-check-for-mac)
        - [Homebrew](#homebrew)
        - [Python 3.9](#python-39)
        - [Pulumi 3.38 or above**](#pulumi-338-or-above)
        - [Git](#git)
        - [Git repo access](#git-repo-access)
        - [TechPass and databaseFederal Account Permission](#techpass-and-databasefederal-account-permission)
        - [SSH key to access TehcOps database federated AWS account EC2 host](#ssh-key-to-access-tehcops-database-federated-aws-account-ec2-host)
        - [zsh vs bash](#zsh-vs-bash)
    - [Running PBD-CLI](#running-pbd-cli)
    - [Running "2.Modify" Menu Functions](#running-2modify-menu-functions)

<!-- /TOC -->

## Additional Dependency Versions

Following are additional software dependency versions which were tested and working

| versions        | Description |
|-----------------|-------------|
| 3.8,3.9 or 3.10 | Python      |
| 3.38 or above   | Pulumi      |
| latest          | Credtool    |

## Pre-requisites

Assumptions for the following procedures:

* Execution from user local machine
* User local machine runs Mac OS
* Mac OS has Python 3.9 or 3.10 installed (brew install python3)
* Infrastructure will be built on Database Platform (PRD1748) AWS federated accounts
* Database installation will be for ScyllaDB and DSE
* User has 'Ops-TechOps' role for all or one of the AWS accounts: tm-nonprod-prd1748, tm-prod-prd1748, tm-pci-prd1748
* User has access to the SSH key files to the respective AWS account and region
* Credtool must be installed in your local. (see Credtool)
    * The credtool secret record for your deployment (ProductName) needs to exist
    * Alternatively you can use pre-existing secrets for testing. Just specify 'PRD2308' as ProductCode, 'casslab' as ProductName, and 'dev' as VPC and set an unique 'DC_ID'.
        * [DBD - Credtool entries for testing Cassandra deployment stacks](https://confluence.livenation.com/x/FkJiEQ)


## Setup and Pre-Check for Mac

### Homebrew

Make sure you have homebrew available on your Mac, if not, following this doc to get homebrew setup https://brew.sh/

Command to verify you have homebrew and update your homebrew

    $ which brew
    /opt/homebrew/bin/brew

    $ brew update

### Python 3.9  

make sure you have Python 3.9 installed.

Command to verify your python version and env setup

    $ python3 --version
    Python 3.8.9

    # brew install python3  will bring newest python version not always python 3.9
    $ brew install python3
    $ python3 --version
    Python 3.10.6

    # sometime even you successful install the new version of python, the python3 --version still point to previous one. In that case,  exit your terminal and open a new window. It will fix the issue.

    $ export PATH=/Users/Linda.Xu/Library/Python/3.10/bin:$PATH 
    $ echo "export PATH=/Users/Linda.Xu/Library/Python/3.10/bin:$PATH" >>~/.bash_profile
    % echo "export PATH=/Users/Linda.Xu/Library/Python/3.10/bin:$PATH" >>~/.zshenv # if you mac is M1 chip, most of case you have zsh instead of bash 

### Pulumi 3.38 or above**

    $ brew install pulumi 
    $ pulumi version
    v3.38.0

### Git

If you have Xcode installed, git should already available on your Mac. Otherwise follow command will trigger installation.

    $ git --version
    git version 2.32.1 (Apple Git-133)

if you never use git, following this doc to setup [Git Basic - Start to use Git](https://confluence.livenation.com/x/jYwcD)


If you have Mac OS upgraded,  you may face to git xcode issue. Error code looks like

    Linda.Xu@Lindas-MacBook-Pro ~ % git
    2022-09-26 12:11:30.642 xcodebuild[33389:14249851] [MT] DVTPlugInLoading: Failed to load code for plug-in com.apple.dt.IDESimulatorAvailability (/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin), error = Error Domain=NSCocoaErrorDomain Code=3588 "dlopen(/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability, 0x0109): Symbol not found: (_OBJC_CLASS_$_SimDiskImage)
    Referenced from: '/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability'
    Expected in: '/Library/Developer/PrivateFrameworks/CoreSimulator.framework/Versions/A/CoreSimulator'" UserInfo={NSLocalizedFailureReason=The bundle couldn’t be loaded., NSLocalizedRecoverySuggestion=Try reinstalling the bundle., NSFilePath=/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability, NSDebugDescription=dlopen(/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability, 0x0109): Symbol not found: (_OBJC_CLASS_$_SimDiskImage)
    Referenced from: '/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability'
    Expected in: '/Library/Developer/PrivateFrameworks/CoreSimulator.framework/Versions/A/CoreSimulator', NSBundlePath=/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin, NSLocalizedDescription=The bundle “IDESimulatorAvailability” couldn’t be loaded.}, dyldError = dlopen(/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability, 0x0000): Symbol not found: (_OBJC_CLASS_$_SimDiskImage)
    Referenced from: '/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin/Contents/MacOS/IDESimulatorAvailability'
    Expected in: '/Library/Developer/PrivateFrameworks/CoreSimulator.framework/Versions/A/CoreSimulator'
    2022-09-26 12:11:30.667 xcodebuild[33389:14249851] [MT] DVTAssertions: ASSERTION FAILURE in /System/Volumes/Data/SWE/Apps/DT/BuildRoots/BuildRoot2/ActiveBuildRoot/Library/Caches/com.apple.xbs/Sources/DVTFrameworks/DVTFrameworks-21303/DVTFoundation/PlugInArchitecture/DataModel/DVTPlugIn.m:374
    Details:  Failed to load code for plug-in com.apple.dt.IDESimulatorAvailability (/Applications/Xcode.app/Contents/PlugIns/IDESimulatorAvailability.ideplugin)
    Please ensure Xcode packages are up-to-date — try running 'xcodebuild -runFirstLaunch'.

Run xcodebuild -runFirstLaunch

    $ xcodebuild -runFirstLaunch


### Git repo access

Note: the repo is private repo, require user grant. Check your permission here https://git.tmaws.io/dbre/pbd/pushbuttondeployer/-/project_members . If you are not in the list,  please reach out **@Aidan Wong**  for grants before start

### TechPass and databaseFederal Account Permission

If you never use TM AWS, please with this documentation to be prepared.  AWS Tooling

If you not use TM TechPass, please follow this document to setup Tech Pass

You need <account>-Ops-TechOps permission on Database Team's federal account to continue the installation. Reach out with Aidan Wong  for the permission

Login to TechPass before you start

### SSH key to access TehcOps database federated AWS account EC2 host

***WARNING:*** Access control base on role is in place here. ssh key access need approval from both **@Erica Ip**  and **@Aidan Wong**.

We have different ssh key for each env. Please make sure you get relative ssh key before install. Refer to this document [AWS ssh key management](https://confluence.livenation.com/x/aC-bDw) for AWS federated account ssh key management.

### zsh vs bash

 ***WARNING***

    If you have new M1 Mac, Z shell (zsh) is be used instead of bash.  You will find a % instead of a $ for command prompt sign

    Linda.Xu@Lindas-MacBook-Pro ~ %

    In Bash env, ~/.bash_profile will be call to setup your env everytime you login to a terminal.

    In Zsh env, ~/.zshenv is the relevant file.

***Note:*** **You may need to copy all env setting from ~/.bash_profile to ~/.zshenv**

Prepare Your Local Environment 

1. Log in AWS with tm Tech Pass
2. Install pulumi
    a. Mac OS:

        # Clone the PBD repository
        $ git clone git@git.tmaws.io:dbre/pbd/pushbuttondeployer.git; cd pushbuttondeployer/pbd-cli; git checkout release-1.2.0
        $ chmod +x pbdcli.sh


## Running PBD-CLI

1. Connect VPN
2. Login in AWS with tm TechPass, if you get token expiration error, logout TechPass and relogin.
3. Execute

    $ pbdcli.sh

4. Choose Your deployment setup:

    [Pulling existing setup]  ...............
    CASSANDRA_VENDOR=dse
    AWS_REGION=us-east-1
    VPC=dev
    PRDCODE=PRD2308
    PRDNAME=dseozzy
    DATA_SIZE=100
    INSTANCE_DATA_TYPE=ebs
    CLUSTER_NAME=cluster1
    DC_ID=dc1
    KEYSPACE_NAME=keyspace1
    ........................................
    Verify the setting, do you want continue with above setting?  (y/Y)

If you do not want to use existing setup. Enter any input other than "Y" and script will ask your setup configuration

    Verify the setting, do you want continue with above setting?  (y/Y) n
    Generate Yaml file for Cassandra Installation
    Cassandra Choice: <scylladb|dse> :scylladb
    AWS Region: <us-east-1|us-west-2|eu-west-1|eu-central-1|ap-southeast-2> us-west-2
    VPC: <dev|qa|qapci|prod|preprod|pciprod|pcipreprod> dev
    Product Code (PRDxxx) : PRD2308
    Product Name: ozzy2
    Data Storage Size (in GB): <100|200|..> 100
    Instance Data Type (ebs|nvme) ebs
    Cassandra Cluster Name: cluster1
    Cassandra Datacenter ID: dc1
    Keyspace Name :appkeyspace
    CASSANDRA_VENDOR=scylladb
    AWS_REGION=us-west-2
    VPC=dev
    PRDCODE=PRD2308
    PRDNAME=ozzy2
    DATA_SIZE=100
    INSTANCE_DATA_TYPE=ebs
    CLUSTER_NAME=cluster1
    DC_ID=dc1
    KEYSPACE_NAME=appkeyspace

5. Choosing release

    Please Input Release Version (for example: release-1.2.0):release-1.2.0
    Press Enter if you want to use the same release for CaC; otherwise, enter the name of the CaC release:
    IaC Release=release-1.2.0, CaC Release=release-1.2.0
    /Users/ozgur.savasan/cassandra-git already exists. Do you want to use existing repo (y/Y):

Enter release full version name. You can choose different version for CaC or just press enter to choose same version. To speed up process you can use existing repo otherwise script will delete existing folder, clone repo and execute pip install.  

6. Main menu

    Please Select:

    1. Install
    2. Modify
    3. Destroy
    0. Quit

    Enter selection [0-9] >

7. Phase 2 functionalites

    1. Install
    2. Modify
    3. Destroy
    0. Quit

    Enter selection [0-9] > 2
    Please Select:

    1. Validate resources on AWS
    2. Preview
    3. Add nodes
    4. Clone DC
    5. Remove DC
    6. Remove one node
    7. Change Instance Type
    9. Back
    0. Quit

    Enter selection [0-9] >

Choose 2 from main menu to access phase 2 functions.

| Menu Item | Function Name             | Description                                                                         |
|-----------|---------------------------|-------------------------------------------------------------------------------------|
| 1         | Validate resources on AWS | Lists resources from AWS directly                                                   |
| 2         | Preview                   | Check Infra (not softwate) Equals "terraform refresh"                               |
| 3         | Add Nodes                 | Add three nodes to selected setup                                                   |
| 4         | Clone DC                  | Asks new datacenter region and dc_id values and clones datacenter                   |
| 5         | Remove DC                 | Asks datacenter name and removes selected datacenter                                |
| 6         | Remove One Node           | Removes one node                                                                    |
| 7         | Change Instance Type      | Asks instance type as free text. Given parameter should be match AWS Instance name. |
| 9         | Back                      | Returns main menu                                                                   |
| 0         | Quit                      | Aborts script                                                                       |

8. To follow running process open a new terminal window and enter the tail command that PBDCLI displays before the process.

    $ You can follow logs in another terminal windows with : tail -25 -f /Users/ozgur.savasan/cassandra-runenv/log/pbd_20230202100548.log
    Press any key to continue...


## Running "2.Modify" Menu Functions 

1. **Validate Resources on AWS**

Lists existing resources from AWS. This method does not lookup database, log files or pulumi stack file, fetches resource informations directly from AWS API via boto3.

2. **Preview**

Executes "pulumi preview" (equivalent function of "terraform preview"), displays a preview of the updates to an existing stack whose state is represented by an existing state file.

3. **Add Notes**

Builds 3 new nodes, installs Cassandra (ScyllaDB/DSE) on new nodes.

See the functions steps [Cassandra User Tools Requirement 2.0#Function1:Horizontalscale-adding3nodesdistributedacrossallaz](https://) .

4. **Clone DC**

Clones Cassandara (ScyllaDB/DSE) cluster to given region and datacenter id. This function builds the exact number of nodes of the existing cluster. When you choose this function, you will be asked for target region and target datacenter id. You may choose same region.

    Please Select:

    1. Validate resources on AWS
    2. Preview
    3. Add nodes
    4. Clone DC
    5. Remove DC
    6. Remove one node
    7. Change Instance Type
    9. Back
    0. Quit

    Enter selection [0-9] > 4
    Cloning Datacenter:
    Target DC-ID : dc2
    Target Region (ex us-east-1): us-west-2

See the function details on [Cassandra User Tools Requirement 2.0#Function2:CloneDC](https://) 

5. **Remove DC**

Removes given datacenter (if exists) from the cluster. When you choose this function you will be asked for full datacenter name (not datacenter id) . This function will not destroy ec2 instances.

    1. Validate resources on AWS
    2. Preview
    3. Add nodes
    4. Clone DC
    5. Remove DC
    6. Remove one node
    7. Change Instance Type
    9. Back
    0. Quit

    Enter selection [0-9] > 5
    Removing Datacenter:
    Enter cluster full dc name. (ex:us-west-2-dev-dc2) :

See the function details on [Cassandra User Tools Requirement 2.0#Function5:Removedcfromcluster](https://) 

6. **Remove One Node**

Removes the given node from the Cassandra cluster.

    Please Select:

    1. Validate resources on AWS
    2. Preview
    3. Add nodes
    4. Clone DC
    5. Remove DC
    6. Remove one node
    7. Change Instance Type
    9. Back
    0. Quit

    Enter selection [0-9] > 6
    Removing node...
    Target Node IP:
    
See the function details on [Cassandra User Tools Requirement 2.0#Function4:Removeonenodefromdc(fornon-seednodeonly)](https://) 

7. **Change Instance Type**

Changes instance type of nodes one by one without stopping Cassandra service. New Instance type will be asked before process, you must choose an [instance type](https://aws.amazon.com/ec2/instance-types/r5/) instance type  from same family.

    Please Select:

    1. Validate resources on AWS
    2. Preview
    3. Add nodes
    4. Clone DC
    5. Remove DC
    6. Remove one node
    7. Change Instance Type
    9. Back
    0. Quit

    Enter selection [0-9] > 7
    Type New Instance Type (ex. r5.2xlarge):

See the function details on [Cassandra User Tools Requirement 2.0#Function3:verticalscaling(changinginstancetype)](https://) 