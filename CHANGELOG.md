# [2.0.0](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v2.0.0) (2023-02-13)


### Continuous Integration

* **packaging:** semantic release packages are installed using package.json ([b32e696](https://gitlab.com/ericklife281/testing_semantic_release/commit/b32e6962e94894412393c03733a9bd0d68fc15a3))


### BREAKING CHANGES

* **packaging:** fixing repo

## [1.5.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v1.5.1) (2023-02-13)

## [1.5.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v1.5.1) (2023-02-13)

## [1.5.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v1.5.1) (2023-02-13)

## [1.5.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v1.5.1) (2023-02-13)

## [1.5.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v1.5.1) (2023-02-13)

## [1.5.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.5.0...v1.5.1) (2023-02-10)

# [1.5.0](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.4.0...v1.5.0) (2023-02-06)


### Features

* **backend:** A addres method is added. ([73febf6](https://gitlab.com/ericklife281/testing_semantic_release/commit/73febf6aabf5c44fcd2e14a9e888abf5bef2e1fe))

# [1.4.0](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.3.1...v1.4.0) (2023-02-06)


### Features

* **ui:** An address field is added to the ui. ([e5fd4e4](https://gitlab.com/ericklife281/testing_semantic_release/commit/e5fd4e44db00a3f0f5c9cf08defb71a9dd6ddd31))

## [1.3.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.3.0...v1.3.1) (2023-02-06)

# [1.3.0](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.2.0...v1.3.0) (2023-02-06)


### Features

* **backend:** A phone method is added. ([55d1841](https://gitlab.com/ericklife281/testing_semantic_release/commit/55d184156a62124d65b98c86450244a65091a055))

# [1.2.0](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.1.1...v1.2.0) (2023-02-06)


### Features

* **ui:** A phone field is added to the ui. ([6ea1641](https://gitlab.com/ericklife281/testing_semantic_release/commit/6ea1641e51056ae8a353dba9c2a5c857145b2fa3))

## [1.1.1](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.1.0...v1.1.1) (2023-02-05)

# [1.1.0](https://gitlab.com/ericklife281/testing_semantic_release/compare/v1.0.0...v1.1.0) (2023-02-02)


### Features

* **flask:** basic flask application is added ([eb6973e](https://gitlab.com/ericklife281/testing_semantic_release/commit/eb6973e04787175aa8d3e7c60ce37c698fe5e88c))
